import {combineReducers, createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import storejs from 'store';

import books from 'apps/books/reducer';
import auth from 'apps/auth/reducer';
import profile from 'apps/profile/reducer';
import alerts from 'apps/alerts/reducer';


var composed;

if (process.env.NODE_ENV === 'production') {
    composed = compose(applyMiddleware(thunk))
} else {
    composed = compose(
        applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
}

const store = createStore(
    combineReducers({
        books,
        auth,
        profile,
        alerts
    }),
    composed
);


export default store;

//for debug
window.store = store;
window.storejs = storejs;

