import _forEach from 'lodash/forEach';
import superagent from 'superagent';
import Cookies from 'cookies-js';
import storejs from 'store';
import {adaptFromApi, adaptToApi} from 'utils/adapter';
import {serializeObjectToQuery} from 'utils/serialize';
import { API_URL, env } from 'envVariables';


function isSuccess(response) {
    return response.body !== null && (
        response.body.status && response.body.status.toUpperCase() === 'SUCCESS' || response.statusCode < 400
    );
}

function isFail(response) {
    return response.body !== null && (
        response.body.status && response.body.status.toUpperCase() === 'FAIL' ||
        response.statusCode >= 400 && response.statusCode < 500
    );
}

function isError(response) {
    return response.body !== null && (
        response.body.status && response.body.status.toUpperCase() === 'Error' || response.statusCode >= 500
    );
}


class AjaxUtil {
    static _splitData = function(initialData) {
        let data = {},
            files = {};
        _forEach(initialData, (value, key) => {
            if (value.constructor === File) {
                files[key] = value;
            } else {
                data[key] = value;
            }
        });
        return [data, files];
    };

    static _sendAjaxRequest = function(method, url, ajaxData, withCSRF, isMultipart = false) {
        // isMultipart - аргумент, указывающий, что в запросе передаются файлы. Однако, похоже на то, что единственный
        // рабочий способ передать файлы с помощью superagent - это НЕ указывать ни Content-Type = 'multipart/form-data'
        // в хедере, ни .type('form') при создании запроса.
        // Таким образом, запрос для передачи файлов ничем не отличается от обычного, а isMultipart не используется.
        let headers = {
            'X-Requested-With': 'XMLHttpRequest',
            //'Content-Type': isMultipart ? 'multipart/form-data' : 'application/json',
            'Accept': 'application/json',
        };

        if(storejs.get('token')) {
            headers['Authorization'] = `JWT ${storejs.get('token')}`
        }

        if (withCSRF) {
            const csrfToken = Cookies.get('csrftoken');
            //ajaxData.csrfmiddlewaretoken = csrfToken;
            headers['X-CSRFToken'] = csrfToken;
        }

        ajaxData = adaptToApi(ajaxData);

        if (isMultipart) {
            let data = new FormData();
            _forEach(ajaxData, (value, key) => {
                data.append(key, value);
            });
            ajaxData = data;
        }

        return new Promise((resolve, reject) => {
            url = (url.indexOf('https://')===0 || url.indexOf('http://')===0) ? url : API_URL+url;
            let request = superagent(method.toUpperCase(), url);
                if(env==='prod') {
                    request = request.withCredentials();
                }
                request
                    .set(headers)
                    //.type(isMultipart ? 'form' : 'json')
                    .send(ajaxData)
                    .end((error, response) => {
                        if (error) {
                            // console.error(`Request to url "${url}" failed.\n${error}\n${JSON.stringify(response)}`);
                            reject(adaptFromApi(error.response.body));
                        } else {
                            let responseBody = adaptFromApi(response.body);
                            if (isSuccess(response)) {
                                resolve(responseBody);
                            } else {
                                reject(responseBody);
                            }
                        }
                    });
        });
    }
}


export function sendGetRequest(url, data) {
    data = data || {};
    return AjaxUtil._sendAjaxRequest('GET', `${url}?${serializeObjectToQuery(data)}`, data, false);
}

export function sendPostRequest(url, data={}, isMultipart=false) {
    return AjaxUtil._sendAjaxRequest('POST', url, data, true, isMultipart);
}

export function sendPutRequest(url, data={}, isMultipart=false) {
    return AjaxUtil._sendAjaxRequest('PUT', url, data, true, isMultipart);
}

export function sendPatchRequest(url, data={}, isMultipart=false) {
    return AjaxUtil._sendAjaxRequest('PATCH', url, data, true, isMultipart);
}

export function sendDeleteRequest(url, data={}) {
    return sendAjaxRequest('DELETE', url, data, true);
}