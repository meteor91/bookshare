export function resolveNewOrderField(orderingFields, field) {
    var fieldIndex = orderingFields.indexOf(field);
    if(fieldIndex<0) {
        fieldIndex = orderingFields.indexOf(`-${field}`);
    }

    if(fieldIndex<0) {
        orderingFields.push(field);
    } else {
        if(orderingFields[fieldIndex]==`-${field}`) {
            orderingFields.splice(fieldIndex, 1);
        } else {
            orderingFields[fieldIndex] = `-${field}`;
        }
    }
}


export function isDescendant(child, parent, includeSelf = false) {
    let node = includeSelf ? child : child.parentNode;
    while (node != null) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}
