import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux'
import {browserHistory} from 'react-router';
import storejs from 'store';

import store from 'redux-store'
import Routes from 'router'
import {verifyToken} from 'apps/auth/action-creators'


class App extends React.Component {
    componentWillMount() {
        store.dispatch(verifyToken(storejs.get('token')))
            .catch(() => {
                storejs.remove('token');
                // browserHistory.push('/');
            });
    }
    render() {
        return (
            <Provider store={store}>
                <Routes/>
            </Provider>
        )
    }
}


render(<App/>, document.getElementById('app'));