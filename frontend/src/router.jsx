import React from 'react';
import {Router, Route, Redirect, browserHistory} from 'react-router';
import {connect} from 'react-redux';

import CommonLayout from 'common/layouts/common';
import AvailableBooksPage from 'apps/books/views/available-books';
import TakeBookPage from 'apps/books/views/take-book';
import ProfilePage from 'apps/profile/views/profile';


class Routes extends React.Component {

    authCheck = (nextState, replace, callback) => {
        if(!this.props.token && !this.props.authorizeRequested) {
            replace('/');
        }
        callback();
    };

    render() {
        return (
            <Router history={browserHistory}>
                <Route component={CommonLayout}>
                    <Route path='/' component={AvailableBooksPage}/>
                    <Route path='/book/:bookId' component={TakeBookPage}/>
                </Route>

                <Route component={CommonLayout} onEnter={this.authCheck}>
                    <Route path="/profile/" component={ProfilePage}/>
                </Route>

               <Redirect from="*" to="/"/>
            </Router>
        );
    }
}

export default connect(
    store => ({
        token: store.auth.token,
        authorizeRequested: store.auth.authorizeRequested
    })
)(Routes)