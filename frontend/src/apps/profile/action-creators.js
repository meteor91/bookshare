import {sendGetRequest, sendPutRequest} from 'utils/http';


export const loadUserBooks = (page=1, order) => {
    return (dispatch, getState) => {
        const params = {
            page,
            order: !!order ? order : getState().books.order
        };
        return sendGetRequest('/api/books/user/', params)
            .then(response => {
                dispatch({
                    type: 'profile/loadBooks',
                    payload: {
                        books: response.results,
                        pagesCount: Math.ceil(response.count/response.pageSize),
                        currentPage: page
                    }
                });
                return Promise.resolve();
            });
    }
};

export const loadUserFinishedLends = (page=1, order) => {
    return (dispatch, getState) => {
        const params = {
            page, finished: true,
            order: !!order ? order : getState().finishedLends.order
        };
        return sendGetRequest('/api/lends/list/', params)
            .then(response => {
                dispatch({
                    type: 'profile/loadFinishedLends',
                    payload: {
                        lends: response.results,
                        pagesCount: Math.ceil(response.count/response.pageSize),
                        currentPage: page
                    }
                });
                return Promise.resolve();
            });
    }
};

export const loadUserActiveLends = (page=1, order) => {
    return (dispatch, getState) => {
        const params = {
            page, finished: false,
            order: !!order ? order : getState().activeLends.order
        };
        return sendGetRequest('/api/lends/list/', params)
            .then(response => {
                dispatch({
                    type: 'profile/loadActiveLends',
                    payload: {
                        lends: response.results,
                        pagesCount: Math.ceil(response.count/response.pageSize),
                        currentPage: page
                    }
                });
                return Promise.resolve();
            });
    }
};

export const finishLend = lendId => {
    return (dispatch, getState) => {
        return sendPutRequest(`/api/lends/finish/${lendId}/`, {finished: true})
            .then(response => {
                if(response.finished) {
                    const apage = getState().profile.activeLends.currentPage;
                    dispatch(loadUserActiveLends(apage));
                    const fpage = getState().profile.finishedLends.currentPage;
                    dispatch(loadUserFinishedLends(fpage));
                    return Promise.resolve(response.finished);
                } else {
                    return Promise.reject(response.finished);
                }

            });
    }
};

export const displayAddBookDialog = (state) => ({
    type: 'profile/displayAddBookDialog',
    payload: {
        displayAddBookDialog: state
    }
});
