import React from 'react';
import {connect} from 'react-redux';

import {finishLend, loadUserActiveLends} from 'apps/profile/action-creators';
import MyBooksTab from '../components/my-books-tab';
import FinishedLendsTab from '../components/finished-lends-tab';
import ActiveLendsTab from '../components/active-lends-tab';


class ProfilePage extends React.Component {
    state = {
        displayAddBookDialog: false,
        bookAdded: new Date(),
        currentTab: 'my-books'
    };

    componentWillMount() {
        const hash = location.hash.substr(1, location.hash.length);
        if(['my-books', 'lends', 'history'].indexOf(hash)>=0) {
            this.setState({
                currentTab: hash
            })
        }
    }

    render () {
        const {currentTab} = this.state;

        return (
            <div>
                <h3>Профиль</h3>

                <ul className="nav nav-tabs">
                    <li onClick={() => (this.handleTabChange('my-books'))}
                        className={currentTab=='my-books' ? 'active' : ''}>
                        <a>Мои Книги</a>
                    </li>
                    <li onClick={() => (this.handleTabChange('lends'))}
                        className={currentTab=='lends' ? 'active' : ''}>
                        <a>Взятые книги</a>
                    </li>
                    <li onClick={() => (this.handleTabChange('history'))}
                        className={currentTab=='history' ? 'active' : ''}>
                        <a>История</a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className={`tab-pane fade ${currentTab=='my-books' ? 'active' : ''} in`}>
                        <MyBooksTab/>
                    </div>
                    <div className={`tab-pane fade ${currentTab=='lends' ? 'active' : ''} in`}>
                        <ActiveLendsTab/>
                    </div>
                    <div  className={`tab-pane fade ${currentTab=='history' ? 'active' : ''} in`}>
                        <FinishedLendsTab/>
                    </div>
                </div>
            </div>
        )
    }

    handleTabChange(tabName) {
        location.hash = '#' + tabName;
        this.setState({
            currentTab: tabName
        })
    };
}

export default connect(
    store => ({
        profile: store.profile
    }),
    null
)(ProfilePage)