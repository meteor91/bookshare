import _assign from 'lodash/assign';
import _forEach from 'lodash/forEach';


const initialState = {
    books: {
        list: [],
        pagesCount: '',
        currentPage: '',
        order: []
    },
    finishedLends: {
        list: [],
        pagesCount: '',
        currentPage: '',
        order: []
    },
    activeLends: {
        list: [],
        pagesCount: '',
        currentPage: '',
        order: []
    },
    user: {
        name: ''
    },
    displayAddBookDialog: false
};

export default (state=initialState, action) => {
    const {type, payload} = action;
    switch (type) {
        case 'profile/loadBooks':
            return _assign({}, state, {
                books: {
                    list: payload.books,
                    pagesCount: payload.pagesCount,
                    currentPage: payload.currentPage,
                    order: payload.order
                }
            });
        case 'profile/loadFinishedLends':
            return _assign({}, state, {
                finishedLends: {
                    list: payload.lends,
                    pagesCount: payload.pagesCount,
                    currentPage: payload.currentPage,
                    order: payload.order
                }
            });
        case 'profile/loadActiveLends':
            return _assign({}, state, {
                activeLends: {
                    list: payload.lends,
                    pagesCount: payload.pagesCount,
                    currentPage: payload.currentPage,
                    order: payload.order
                }
            });
        case 'profile/setUserData':
            return _assign({}, state, {
                user: payload.user
            });
        case 'profile/displayAddBookDialog':
            return _assign({}, state, {
                displayAddBookDialog: payload.displayAddBookDialog
            });
        case 'profile/finishLend':
            const {id, finishedAt} = payload;
            const lends = _assign({}, state.lends);
            _forEach(lends.list, item => {
                if (item.id==id) {
                    item.finished=true;
                    item.finishedAt = finishedAt;
                    return false;
                }
            });
            return _assign({}, state, {lends});
        default:
            return state;
    }
}