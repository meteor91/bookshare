import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import ListGrid from 'common/components/list-grid';
import {createAlert} from 'apps/alerts/action-creators';
import {finishLend, loadUserActiveLends} from 'apps/profile/action-creators';


const Columns = [
    {label: 'Автор', value: 'user_book__book__author__name', width: 25},
    {label: 'Название', value: 'user_book__book__name', width: 25},
    {label: 'Владелец', value: 'user_book__user__name', width: 20},
    {label: 'Книга взята', value: 'begin_at', width: 20},
    {label: '', width: 10},
];

function ActiveLendRow({item, handleFinishLend}) {
    return (
        <tr>
            <td>{item.userBook.book.author.name}</td>
            <td>{item.userBook.book.name}</td>
            <td>{item.userBook.user.name}</td>
            <td>{moment(item.beginAt).locale('ru').format('ll')}</td>
            <td>
                <a onClick={e => handleFinishLend(e, item.id)} className="btn btn-info btn-xs">
                    Вернуть
                </a>
            </td>
        </tr>
    )
}


class ActiveLendsTab extends React.Component {

    render() {
        const rowAdditionalProps = {
            handleFinishLend: this.handleFinishLend
        };

        return (
            <ListGrid
                columns={Columns}
                Row={ActiveLendRow}
                rowAdditionalProps={rowAdditionalProps}
                loadData={this.props.loadData}
                data={this.props.data}
            />
        )
    }

    handleFinishLend = (e, lendId) => {
        e.preventDefault();
        this.props.finishLend(lendId)
            .then(() => {
                this.props.createAlert('Книга возвращена владельцу.', 'success');
            }, () => {
                this.props.createAlert('Что то пошло не так, попробуйте позже.', 'danger');
            });
    }
}

export default connect(
    store => ({
        data: store.profile.activeLends
    }),
    (dispatch, ownProps) => ({
        finishLend: lendId => (dispatch(finishLend(lendId))),
        createAlert: (message, type) => (dispatch(createAlert(message, type))),
        loadData: (page, order) => (dispatch(loadUserActiveLends(page, order))),
    })
)(ActiveLendsTab)