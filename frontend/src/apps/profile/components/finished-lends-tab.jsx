import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import ListGrid from 'common/components/list-grid';
import {finishLend, loadUserFinishedLends} from 'apps/profile/action-creators';

const Columns = [
    {label: 'Автор', value: 'user_book__book__author__name', width: 20},
    {label: 'Название', value: 'user_book__book__name', width: 24},
    {label: 'Владелец', value: 'user_book__user__name', width: 20},
    {label: 'Книга взята', value: 'begin_at', width: 18},
    {label: 'Возвращена', value: 'finished_at', width: 18},
];


function FinishedLendRow({item}) {
    return (
        <tr>
            <td>{item.userBook.book.author.name}</td>
            <td>{item.userBook.book.name}</td>
            <td>{item.userBook.user.name}</td>
            <td>{moment(item.beginAt).locale('ru').format('ll')}</td>
            <td>{moment(item.finishedAt).locale('ru').format('ll')}</td>
        </tr>
    )
}

class FinishedLendsTab extends React.Component {

    render() {
        return (
            <ListGrid
                columns={Columns}
                Row={FinishedLendRow}
                loadData={this.props.loadData}
                data={this.props.data}
            />
        )
    }

}

export default connect(
    store => ({
        data: store.profile.finishedLends
    }),
    dispatch => ({
        loadData: (page, order) => (
            dispatch(loadUserFinishedLends(page, order))
        ),
    })
)(FinishedLendsTab)