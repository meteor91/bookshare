import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {loadUserBooks} from 'apps/profile/action-creators';
import ListGrid from 'common/components/list-grid';
import BookForm from '../components/book-form';


const Columns = [
    {label: 'Автор', value: 'book__author__name', width: 30},
    {label: 'Название', value: 'book__name', width: 30},
    {label: 'Добавлен', value: 'created_at', width: 25},
    {label: 'Статус', width: 15},
];

function MyBookRow({item, handleUpdateButtonClick}) {
    const updateClick = () => handleUpdateButtonClick(item.id);
    return (
        <tr key={`book-${item.id}`}>
            <td>{item.book.author.name}</td>
            <td>{item.book.name}</td>
            <td>{moment(item.book.createdAt).locale('ru').format('lll')}</td>
            <td>
                {item.lend !== null ?
                    <div>
                        У пользователя {item.lend.receiver.name} <br/>
                        Будет возвращена не позднее <br/>
                        {moment(item.lend.latestReturnDate).locale('ru').format('lll')}
                    </div>
                    :
                    'У вас'
                }
            </td>
            <td>
                {item.lend !== null ?
                    <button disabled className="btn btn-default">
                        ред.
                    </button>
                    :
                    <button onClick={updateClick} className="btn btn-info">
                        ред.
                    </button>
                }
            </td>
        </tr>
    )
}


class MyBooksTab extends React.Component {

    state = {
        displayAddBookDialog: false,
        book: null
    };

    render() {
        const rowAdditionalProps = {
            handleUpdateButtonClick: this.handleUpdateButtonClick
        };

        return (
            <div>
                <ListGrid
                    columns={Columns}
                    Row={MyBookRow}
                    rowAdditionalProps={rowAdditionalProps}
                    loadData={this.props.loadData}
                    data={this.props.data}
                />

                <div>
                    <button onClick={this.addBookDialogShow} className="btn btn-info">Добавить книгу</button>
                </div>

                <BookForm
                    display={this.state.displayAddBookDialog}
                    book={this.state.book}
                    handleClose={this.addBookDialogClose}
                />
            </div>
        )
    }

    handleUpdateButtonClick = (id) => {
        const book = this.props.data.list.find(book => (book.id === id));
        this.setState({
            displayAddBookDialog: true,
            book
        });
    };

    addBookDialogShow = () => {
        this.setState({displayAddBookDialog: true});
    };

    addBookDialogClose = () => {
        this.setState({
            displayAddBookDialog: false,
            book: null
        });
    };
}

export default connect(
    store => ({
        data: store.profile.books
    }),
    dispatch => ({
        loadData: (page, order) => (dispatch(loadUserBooks(page, order)))
    })
)(MyBooksTab)