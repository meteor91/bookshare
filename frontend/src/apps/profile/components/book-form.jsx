import React from 'react';
import {connect} from 'react-redux';

import ModalLayout from 'common/layouts/modal';
import SearchInput from 'common/components/inputs/search-input';
import FormField from 'common/components/inputs/forms/form-field';
import {searchAuthor, searchBook} from 'apps/books/action-creators';
import {createBook, updateBook} from 'apps/books/action-creators';
import {createAlert} from 'apps/alerts/action-creators';

class BookForm extends React.Component {

    static propTypes = {
        display: React.PropTypes.bool,
        handleClose: React.PropTypes.func,
        book: React.PropTypes.object
    };

    state = {
        book: {
            authorName: '',
            bookName: '',
            maxLendDays: '7',
        },
        errors: {},
        isUpdate: false
    };

    componentWillReceiveProps(props) {
        if(props.book) {
            this.setState({
                book: {
                    authorName: props.book.book.author.name,
                    bookName: props.book.book.name,
                    maxLendDays: props.book.maxLendDays,
                },
                isUpdate: true
            });
        } else {
            this.clearState();
        }
    }

    render() {
        const { book, isUpdate } = this.state;
        return (
            <ModalLayout display={this.props.display} handleClose={this.handleCancelClick}>
                <div onClick={this.handleClick}>
                    <div className="modal-header">
                        { isUpdate ? 'Редактировать книгу' : 'Добавить книгу' }
                    </div>
                    <div className="modal-body">
                        <div className="row">
                            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                                <fieldset>
                                    <FormField label="Автор" errors={this.getFieldErrors('authorName')}>
                                        <SearchInput className="form-control" fieldName="name"
                                                     value={book.authorName}
                                                     handleChange={this.handleAuthorChange}
                                                     placeholder="Автор" searchHandler={searchAuthor}/>
                                    </FormField>

                                    <FormField label="Название книги" errors={this.getFieldErrors('bookName')}>
                                        <SearchInput className="form-control" fieldName="name"
                                                     value={book.bookName}
                                                     handleChange={this.handleBookChange}
                                                     placeholder="Название книги" searchHandler={searchBook}/>
                                    </FormField>

                                    <FormField label="Максимальный срок передачи" errors={this.getFieldErrors('maxLendDays')}>
                                        <input type="text" className="form-control"
                                               value={book.maxLendDays}
                                               onChange={this.handleMaxLendDays}
                                               placeholder="Количество дней"/>
                                    </FormField>

                                    <div className="form-group">
                                        <div className="col-lg-10 col-lg-offset-2">
                                            <button type="submit" className="btn btn-primary">Сохранить</button>
                                            <button onClick={this.handleCancelClick} className="btn btn-default m10">
                                                Отмена
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </ModalLayout>
        )
    }

    getFieldErrors(fieldName) {
        if(fieldName in this.state.errors) {
            return this.state.errors[fieldName];
        }
        return []
    }

    clearState() {
        this.setState({
            book: {
                authorName: '',
                bookName: '',
                maxLendDays: 7,
            },
            errors: {},
            isUpdate: false
        });
    }

    handleClick(e) {
        e.stopPropagation();
    };

    handleCancelClick = (e) => {
        e.preventDefault();
        this.clearState();
        this.props.handleClose();
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let action;

        if(this.state.isUpdate) {
            action = this.props.updateBook(this.state.book, this.props.book.id)
                .then(response => {
                    this.props.createAlert(
                        `Книга <u><b>${response.bookName} - ${response.authorName}</b></u> обновлена.`,
                        'success'
                    );
                    this.props.handleClose();
                });
        } else {
            action = this.props.createBook(this.state.book)
                .then(response => {
                    this.props.createAlert(
                        `Книга <u><b>${response.bookName} - ${response.authorName}</b></u> успешно добавлена.`,
                        'success'
                    );
                    this.props.handleClose();
                });
        }

        action
            .catch(response => {
                if(response) {
                    this.setState({
                        errors: response
                    });
                } else {
                    this.props.createAlert('Что то пошло не так, попробуйте позже', 'danger');
                    this.props.handleClose();
                }
            });
    };

    handleAuthorChange = (authorName) => {
        this.setState({book: {...this.state.book, authorName}});
    };

    handleBookChange = (bookName) => {
        this.setState({book: {...this.state.book, bookName}});
    };

    handleMaxLendDays = (e) => {
        const maxLendDays = e.target.value;
        this.setState({book: {...this.state.book, maxLendDays}});
    };
}

export default connect(
    null,
    dispatch => ({
        createBook: data => (dispatch(createBook(data))),
        updateBook: (data, id) => (dispatch(updateBook(data, id))),
        createAlert: (message, type) => (dispatch(createAlert(message, type)))
    })
)(BookForm)