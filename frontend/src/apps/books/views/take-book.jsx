import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {declOfNum} from 'utils/lang';
import {loadBookDetail, takeBook} from 'apps/books/action-creators';
import {displayAuthDialog} from 'apps/auth/action-creators';
import {createAlert} from 'apps/alerts/action-creators';
import TakeBookDialog from 'apps/books/components/take-book-dialog.jsx';
import Loader from 'common/components/loader';
import NotFound from 'common/components/not-found';

const TakenByMe = ({username, dateFree}) => (
    <div>
        Вы брали эту книгу у пользователя <i>{username}</i>, не забудьте вернуть ее до {dateFree}
    </div>
);

const AlreadyTaken = ({username, dateFree}) => (
    <div>
        Книга пользователя <i>{username}</i> будет доступна не позже {dateFree}
    </div>
);


class TakeBookPage extends React.Component {

    state = {
        displayTakeBookDialog: false,
        userBook: {}
    };

    componentWillMount() {
        const bookId = this.props.routeParams.bookId;
        this.props.loadBookDetail(bookId);
    }


    render() {
        const bookDetails = this.props.bookDetails,
              bookId = this.props.routeParams.bookId,
              loadStatus = bookId in bookDetails ? bookDetails[bookId].loadStatus : 'requested';

        switch (loadStatus) {
            case 'requested':
                return <Loader/>;
            case 'failed':
                return <NotFound/>;
            case 'success':
                return this.renderSuccess();
            default:
                return (
                    <div>Что то пошло не так</div>
                )
        }
    }

    renderSuccess() {
        const bookDetails = this.props.bookDetails,
              bookId = this.props.routeParams.bookId,
              book = bookDetails[bookId].data;
        return (
            <div>
                <h1>Взять книгу {`${book.name}`}</h1>
                <ul className="list-group">
                    {book.userBookInfo.map((item, index) => (
                        this.renderUserBookInfo(book, item)
                    ))}
                </ul>
                <TakeBookDialog
                    handleClose={this.closeDialog}
                    userBook={this.state.userBook}
                    bookInfo={this.state.bookInfo}
                    display={this.state.displayTakeBookDialog && this.props.authorized}
                    handleAgree={this.confirmTakeBook}/>

            </div>
        )
    }

    renderUserBookInfo(book, userBookInfo) {
        const userBook = userBookInfo.userBook;
        if(userBookInfo.available==false) {
            var text;
            if(userBookInfo.lendByMe) {
                text = <TakenByMe username={userBook.user.name}
                                  dateFree={moment(userBookInfo.dateFree).locale('ru').format('ll')}/>;
            } else {
                text = <AlreadyTaken username={userBook.user.name}
                                     dateFree={moment(userBookInfo.dateFree).locale('ru').format('ll')}/>;
            }
            return (
                <li key={`userbook-${userBook.id}`} className="list-group-item">
                    <h4 className="list-group-item-heading">{text}</h4>
                </li>
            )
        } else {
            return (
                <li key={`userbook-${userBook.id}`} className="list-group-item">
                    <h4 className="list-group-item-heading">Пользователь <i>{userBook.user.name}</i></h4>
                    <p className="list-group-item-text">
                        Готов дать книгу
                        на {userBook.maxLendDays} {declOfNum(userBook.maxLendDays, ['день', 'дня', 'дней'])}
                    </p>
                    <a className="btn btn-info m-t-20"
                       onClick={(e) => (this.handleTakeBookButton(e, userBook, book.name, book.author.name))}>
                        Одолжить
                    </a>
                </li>
            )
        }
    }

    handleTakeBookButton = (e, userBook, bookName, authorName) => {
        e.preventDefault();
        if(!this.props.authorized) {
            this.props.displayAuthDialog();
        }
        this.setState({
            displayTakeBookDialog: true,
            userBook,
            bookInfo: {
                bookName,
                authorName
            }
        });
    };

    confirmTakeBook = userBookId => {
        this.props.takeBook(userBookId)
            .then(() => {
                const bookId = this.props.routeParams.bookId;
                this.props.loadBookDetail(bookId);
                this.props.createAlert('Вы взяли книгу, приятного чтения!', 'success');
                this.closeDialog();
            }, () => {
                this.props.createAlert('Что то пошло не так, попробуйте позже!', 'danger');
                this.closeDialog();
            });
    };

    closeDialog = () => {
        this.setState({
            displayTakeBookDialog: false
        })
    };
}

export default connect(
    store => ({
        bookDetails: store.books.bookDetails,
        authorized: store.auth.authorized,
    }),
    dispatch => ({
        loadBookDetail: bookId => (dispatch(loadBookDetail(bookId))),
        displayAuthDialog: callback => (dispatch(displayAuthDialog(true))),
        takeBook: userBookId => (dispatch(takeBook(userBookId))),
        createAlert: (message, type) => (dispatch(createAlert(message, type)))
    })
)(TakeBookPage)