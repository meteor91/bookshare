import React from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';

import {loadAvailableBooks} from 'apps/books/action-creators';
import ListGrid from 'common/components/list-grid';

const Columns = [
    {label: 'Автор', value: 'author__name', width: 30},
    {label: 'Название', value: 'name', width: 70},
];

function AvailableBookRow({item, index}) {
    return (
        <tr key={`book-${index}`}>
            <td>{item.author.name}</td>
            <td><Link to={`/book/${item.id}`}>{item.name}</Link></td>
        </tr>
    )
}

class AvailableBooks extends React.Component {
    componentWillReceiveProps(newProps) {
        if(newProps.authorized !== this.props.authorized) {
            this.props.loadData();
        }
    }

    render() {
        return (
            <div>
                <h1>Список доступных книг</h1>
                <p className="lead">
                    Доступные книги<br/>
                </p>
                <ListGrid
                    columns={Columns}
                    Row={AvailableBookRow}
                    authorized={this.props.authorized}
                    data={this.props.data}
                    loadData={this.props.loadData}
                />
            </div>
        )
    }

}

export default connect(
    store => ({
        authorized: store.auth.authorized,
        data: store.books.availableBooks,
    }),
    dispatch => ({
        loadData: (page, params) => (dispatch(loadAvailableBooks(page, params))),
    })
)(AvailableBooks)