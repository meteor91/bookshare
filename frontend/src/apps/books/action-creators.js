import {sendGetRequest, sendPutRequest, sendPostRequest} from 'utils/http';
import {createAlert} from  'apps/alerts/action-creators';
import {loadUserBooks} from 'apps/profile/action-creators';

export const loadAvailableBooks = (page=1, order) => {
    return (dispatch, getState) => {
        const params = {
            page,
            order: !!order ? order : getState().books.availableBooks.order
        };
        return sendGetRequest('/api/books/available/', params)
            .then(response => {
                dispatch({
                    type: 'books/loadBooks',
                    payload: {
                        books: response.results,
                        pagesCount: Math.ceil(response.count / response.pageSize),
                        currentPage: page,
                        order
                    }
                });
            });
    }
};

export const loadBookDetail = (bookId) => {
    return dispatch => {
        dispatch({
            type: 'books/loadBookDetail',
            payload: {
                loadStatus: 'requested',
                id: bookId
            }
        });
        sendGetRequest(`/api/books/book/${bookId}/`)
            .then(response => {
                dispatch({
                    type: 'books/loadBookDetail',
                    payload: {
                        data: response,
                        loadStatus: 'success',
                        id: bookId
                    }
                });
            }, () => {
                dispatch({
                    type: 'books/loadBookDetail',
                    payload: {
                        loadStatus: 'failed',
                        id: bookId
                    }
                });
            });
    }
};

export const searchAuthor = (name) => {
    return sendGetRequest(`/api/books/author/search/${name}/`);
};

export const searchBook = (name) => {
    return sendGetRequest(`/api/books/search/${name}/`);
};

export const createBook = (data) => {
    return dispatch => {
        return sendPostRequest('/api/books/create/', data);
    }
};

export const updateBook = (data, id) => {
    return dispatch => {
        return sendPutRequest(`/api/books/update/${id}/`, data)
            .then(response => {
                console.log(response);
                dispatch(loadUserBooks());
                return Promise.resolve(response);
            });
    }
};

export const takeBook = (userBook) => {
    return dispatch => {
        return sendPostRequest('/api/lends/create/', {userBook});
    }
}