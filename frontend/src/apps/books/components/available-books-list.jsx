import React from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';

import ListGrid from 'common/components/list-grid';
import {loadAvailableBooks} from 'apps/books/action-creators';
import {displayAuthDialog} from 'apps/auth/action-creators';

class AvailableBooksList extends ListGrid {

    constructor(props) {
        super(props);
        this.state = {
            ...this.state,
            columns: [
                {label: 'Автор', value: 'author__name'},
                {label: 'Название', value: 'name'},
            ]
        };
    }

    componentWillMount() {
        if (this.props.authorized!=null) {
            this.handleChangePage(1);
        }
    }

    componentWillReceiveProps(nexProps) {
        if (nexProps.authorized != this.props.authorized) {
            this.handleChangePage(1);
        }
    }

    renderRow(item, index) {
        return (
            <tr key={`book-${index}`}>
                <td>{item.author.name}</td>
                <td><Link to={`/book/${item.id}`}>{item.name}</Link></td>
            </tr>
        )
    }
}

export default connect(
    store => ({
        data: store.books.availableBooks,
    }),
    dispatch => ({
        loadData: (page, params) => (dispatch(loadAvailableBooks(page, params))),
    })
)(AvailableBooksList)