import React from 'react';

import ModalLayout from 'common/layouts/modal';
import {declOfNum} from 'utils/lang';

export default class TakeBookDialog extends React.Component {

    static propTypes = {
        display: React.PropTypes.bool,
        handleClose: React.PropTypes.func,
        userBook: React.PropTypes.object,
        bookInfo: React.PropTypes.shape({
            authorName: React.PropTypes.string,
            bookName: React.PropTypes.string
        })
    };

    state = {
        maxLendDays: '7',
        errors: {}
    };

    render() {
        return (
            <ModalLayout display={this.props.display} handleClose={this.handleCancelClick}>
                <div onClick={this.handleClick}>
                    <div className="modal-body">
                        {this.renderMessage()}
                    </div>
                </div>
            </ModalLayout>
        )
    }

    renderMessage() {
        if(this.props.display) {
            const {authorName, bookName} = this.props.bookInfo,
                  userBook = this.props.userBook;

            return (
                <div>
                    <div className="row">
                        <p>
                            Одолжить книгу <i><u>{authorName} - {bookName}</u></i> у пользователя {userBook.user.name}.
                        </p>
                        <p className="text-warning">
                            Вам необходимо будет вернуть книгу в течении {userBook.maxLendDays} {declOfNum(userBook.maxLendDays, ['день', 'дня', 'дней'])}.
                        </p>
                    </div>
                    <div className="row m-t-20">
                        <a onClick={this.handleAgreeClick} className="btn btn-success">Беру!</a>
                        <a onClick={this.handleCancelClick} className="btn btn-default m10">Я передумал</a>
                    </div>
                </div>
            )
        } else return null;
    }

    handleClick(e) {
        e.stopPropagation();
    };

    handleAgreeClick = (e) => {
        e.preventDefault();
        this.props.handleAgree(this.props.userBook.id);
    };

    handleCancelClick = (e) => {
        e.preventDefault();
        this.props.handleClose()
    };

    handleSubmit = (e) => {
        e.preventDefault();
    };

    getFieldErrors(fieldName) {
        if(fieldName in this.state.errors) {
            return this.state.errors[fieldName];
        }
        return []
    }
}