import _assign from 'lodash/assign';


const initialState = {
    availableBooks: {
        list: [],
        pagesCount: '',
        currentPage: '',
        order: []
    },
    bookDetails: {},
    currentPage: 1,
    pagesCount: 0
};

export default function(state=initialState, action) {
    const {type, payload} = action;
    switch (type) {
        case 'books/loadBooks':
            return _assign({}, state, {
                availableBooks: {
                    list: payload.books,
                    pagesCount: payload.pagesCount,
                    currentPage: payload.currentPage,
                    order: payload.order
                }
            });

        case 'books/loadBookDetail':
            const bookDetails = _assign({}, state.bookDetails);
            bookDetails[payload.id] = payload;
            return _assign({}, state, {bookDetails});

        default:
            return state;
    }
}