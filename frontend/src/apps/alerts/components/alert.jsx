import React from 'react';


export default ({message, handleClose, type}) => {
    return (
        <div className="bs-component">
            <div className={`alert alert-dismissible alert-${type}`}>
                <button type="button" className="close" onClick={handleClose}>×</button>
                <span dangerouslySetInnerHTML={{__html: message}}></span>
            </div>
        </div>
    )
}
