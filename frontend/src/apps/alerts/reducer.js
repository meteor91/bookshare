import _assign from 'lodash/assign';
import _pickBy from 'lodash/pickBy'

const initialState = {};

export default function(state=initialState, action) {
    switch (action.type) {
        case 'alerts/close':
            return _pickBy(state, (value, key) => {
                return key!=action.payload.id
            });
            break;
        case 'alerts/create':
            const alerts = {};
            alerts[action.payload.id] = {
                message: action.payload.message,
                type: action.payload.type,
                read: false
            };
            return _assign({}, state, alerts);
            break;
        default:
            return state;
    }
}