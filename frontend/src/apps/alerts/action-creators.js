export const createAlert = (message, type) => ({
    type: 'alerts/create',
    payload: {
        message,
        type,
        id: (new Date()).getTime()
    }
});

export const closeAlert = (id) => ({
    type: 'alerts/close',
    payload: {
        id
    }
});