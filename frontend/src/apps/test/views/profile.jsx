import React from 'react';
import {connect} from 'react-redux';


class UserInfo extends React.Component {

    componentWillReceiveProps(nextProps) {
        console.log('next props');
    }

    render() {
        return (
            <div>{this.props.user.name}</div>
        )
    }
}
console.log('here we go!');

UserInfo = connect(
    store => ({
        user: store.profile.user
    })
)(UserInfo);

class TestPage extends React.Component {

    state = {
        current:0
    };

    render () {
        console.log('rendered');
        return (
            <div>
                <h3>Test page</h3>
                <div>{this.props.test.count}</div>
                <div>{this.state.current}</div>
                <div>
                    <button onClick={()=>(this.props.inc())}>+</button>
                    <button onClick={()=>(this.props.dec())}>-</button>
                    <button onClick={()=>(console.log(TestPage.someProp))}>piu</button>
                    <button onClick={this.updateCurrent}>UPD</button>
                </div>
                <div>
                    <UserInfo/>
                </div>
            </div>
        )
    }

    updateCurrent = () => {
        this.setState({
            current: this.props.test.count
        })
    }

}


export default connect(
    store => ({
        test: store.test
    }),
    dispatch => ({
        inc: () => (dispatch({type: 'test/inc'})),
        dec: () => (dispatch({type: 'test/dec'}))
    })
)(TestPage)

TestPage.someProp='go';