import _assign from 'lodash/assign';


const initialState = {
    count: 0
};

export default function(state=initialState, action) {
    const {type} = action;
    switch (type) {
        case 'test/inc':
            return _assign({}, state, {
                count: state.count+=1
            });

        case 'test/dec':
            return _assign({}, state, {
                count: state.count-=1
            });

        default:
            return state;
    }
}