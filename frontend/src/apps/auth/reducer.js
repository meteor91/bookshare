import _assign from 'lodash/assign';


const initialState = {
    token: '',
    // authorized нужно изменить чтобы принимал минимум 3 понятных значения:
    //   1) статус авторизации в процессе определения
    //   2) авторизован
    //   3) не аворизован
    // для простоты пока оставлен вариант с null, true, false
    authorized: null,
    authorizeRequested: false,
    displayAuthDialog: false,
    user: {
        login: '',
        name: '',
        expires: ''
    }
};

export default function(state=initialState, action) {
    switch (action.type) {
        case 'auth/setAuth':
            return _assign({}, state, {
                authorized: action.payload.authorized
            });
        case 'auth/setToken':
            return _assign({}, state, {
                token: action.payload.token,
            });

        case 'auth/authRequested':
            return _assign({}, state, {
                authorizeRequested: action.payload.authorizeRequested
            });

        case 'auth/logout':
            return _assign({}, state, {
                token: null,
                authorized: false,
                user: initialState.user
            });

        case 'auth/displayAuthDialog':
            return _assign({}, state, {
                displayAuthDialog:  action.payload.dialogState
            });

        default:
            return state;
    }
}