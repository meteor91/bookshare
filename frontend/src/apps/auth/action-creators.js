import storejs from 'store';
import {sendPostRequest} from 'utils/http';


export const displayAuthDialog = (dialogState) => {
    return {
        type: 'auth/displayAuthDialog',
        payload: {
            dialogState
        }
    }
};


export const authRequested = (requested) => ({
    type: 'auth/authRequested',
    payload: {
        authorizeRequested: requested
    }
});

export const setAuth = (state) => ({
    type: 'auth/setAuth',
    payload: {
        authorized: state
    }
});

export const setToken = (token) => ({
    type: 'auth/setToken',
    payload: {token}
});

export const socialAuth = (provider, accessToken) => {
    return dispatch => {
        return sendPostRequest('/api/user/social/auth/', {provider, accessToken})
            .then(response => {
                dispatch(setToken(response.token));
                storejs.set('token', response.token);

                dispatch(setAuth(true));
                dispatch(displayAuthDialog(false));
                const user = parseToken(response.token);
                dispatch({type: 'profile/setUserData', payload: {user}});
                return Promise.resolve();
            });
    }
};

export const verifyToken = (token) => {
    return dispatch => {
        if(token==null) {
            dispatch(setAuth(false));
            return Promise.reject('no_token');
        }
        dispatch(authRequested(true));
        return sendPostRequest('/api/user/api-token-verify/', {token})
            .then(response => {
                dispatch(authRequested(false));
                if('token' in response && response.token == token) {
                    const user = parseToken(response.token);
                    dispatch({type: 'profile/setUserData', payload: {user}});
                    dispatch(setToken(response.token));
                    dispatch(setAuth(true));
                } else {
                    dispatch(setAuth(false));
                    return Promise.reject('incorrect_token');
                }
            });
    }
};

export const authorize = (token) => ({
    type: 'auth/auth',
    payload: {token}
});

export const logout = () => {
    storejs.remove('token');
    return {type: 'auth/logout'};
};

function parseToken(token) {
    const parts = token.split('.');
    if(parts.length==3) {
        return JSON.parse(atob(parts[1]))
    }
}