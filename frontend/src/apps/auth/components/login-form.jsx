import React from 'react';
import FacebookLogin from 'react-facebook-login';
import {connect} from 'react-redux';

import {createAlert} from 'apps/alerts/action-creators';
import {socialAuth} from '../action-creators'
import ModalLayout from 'common/layouts/modal'
import Loader from 'common/components/loader';

class LoginForm extends React.Component {
    state = {
        displayLoading: false
    };

    render() {
        const displayLoading = this.state.displayLoading;
        return (
            <ModalLayout display={this.props.display} handleClose={this.props.handleClose}>
                <div onClick={this.handleClick}>
                    <div className="modal-header">
                        {displayLoading ? 'Идет вход, секунду...' : 'Войти в Bookshare через'}
                    </div>
                    <div className={`modal-body ${displayLoading ? 'display-none' : ''}`}>
                        <div className="row mg-btm">
                            <div className="col-md-12">
                                <FacebookLogin
                                    appId="1686509614994500"
                                    fields="name"
                                    callback={this.handleFBLogin}
                                    textButton="Facebook"
                                    cssClass="btn btn-block btn-social btn-facebook"
                                    icon={<i className="fa fa-facebook"></i>}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <a className="btn btn-block btn-social btn-vk" onClick={this.handleVKLogin}>
                                    <i className="fa fa-vk"></i> ВКонтакте
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className={`modal-body ${displayLoading ? '' : 'display-none'}`}>
                        <Loader/>
                    </div>
                </div>
            </ModalLayout>
        )
    }

    handleClick(e) {
        e.stopPropagation();
    }

    handleClose = () => {
        if(!this.state.displayLoading) {
            this.props.handleClose();
        }
    };

    handleVKLogin = () => {
        VK.Auth.login((e) => {
            if(e.status=='connected') {
                this.setState({displayLoading: true});
                this.props.auth('vk', e.session.sid)
                    .then(() => (this.props.handleClose()));
            } else {
                this.props.handleClose();
                this.props.createAlert('Что то пошло не так, попробуйте позже', 'danger');
            }
        }, 1);
    };

    handleFBLogin = (e) => {
        if(e.status=='not_authorized') {
            this.props.handleClose();
            this.props.createAlert('Что то пошло не так, попробуйте позже', 'danger');
        } else {
            this.setState({displayLoading: true});
            this.props.auth('fb', e.accessToken)
                .then(() => (this.props.handleClose()));
        }

    };
}

export default connect(
    null,
    dispatch => ({
        auth: (provider, accessToken) => (dispatch(socialAuth(provider, accessToken))),
        createAlert: (message, type) => (dispatch(createAlert(message, type)))
    })
)(LoginForm)