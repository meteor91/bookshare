import React from 'react';
import {Link} from 'react-router';

export default () => {
    return (
        <div className="error404">
            <div className="error404-code m-b-10 m-t-20">404</div>
            <h3 className="font-bold">Страница не найдена...</h3>
    
            <div className="error404-desc">
                Извините, страница которую вы ищите была удалена или вовсе не существует.<br/>
                Попробуйте обновить страницу или вернитесь на главную.
                <div>
                    <Link to="/" className="login-detail-panel-button btn">
                            <i className="fa fa-arrow-left"></i>
                            Вернуться на главную
                    </Link>
                </div>
            </div>
        </div>
    )
}