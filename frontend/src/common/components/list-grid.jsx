import React from 'react';

import {resolveNewOrderField} from 'utils/tools'
import Pagination from 'common/components/pagination';
import Loader from 'common/components/loader';


export default class ListGrid extends React.Component {

    static propTypes = {
        initialOrder: React.PropTypes.array,
        data: React.PropTypes.shape({
            currentPage: React.PropTypes.any.isRequired,
            pagesCount: React.PropTypes.any.isRequired,
        }),
        rowAdditionalProps: React.PropTypes.object,
        loadData: React.PropTypes.func.isRequired,
        authorized: React.PropTypes.bool,
    };

    constructor(props) {
        super(props);
        const state = {isLoading: true};
        if('initialOrder' in props) {
            state.order = props.initialOrder;
        } else {
            state.order = [];
        }
        this.state = state;
    }

    componentWillMount() {
        this.handleChangePage(1);
    }

    render() {
        const {list, pagesCount, currentPage} = this.props.data,
              {Row, columns, rowAdditionalProps} = this.props,
              {isLoading, order} = this.state;
        return (
            <div className="bs-component">
                {isLoading ?
                    <Loader/>
                        :
                    <table className="table table-striped table-hover">
                        {this.renderColgroup(columns)}
                        {this.renderHead(columns, order)}
                        <tbody>
                            {list.map((item, index) => (
                                <Row item={item} key={`row-${index}`} {...rowAdditionalProps}/>
                            ))}
                        </tbody>
                    </table>
                }
                {pagesCount > 1 && <Pagination pagesCount={pagesCount} currentPage={currentPage} changePage={this.handleChangePage}/>}
            </div>
        )
    }

    renderColgroup(columns) {
        const colWidths = [];
        columns.forEach(column => {
            if('width' in column) {
                colWidths.push(column.width)
            }
        });

        return (
            <colgroup>
                {colWidths.map((item, index) => (
                    <col key={`col-${index}`} style={{width: `${item}%`}}/>
                ))}
            </colgroup>
        )
    }

    renderHead(columns, order) {
        return (
            <thead>
                <tr>
                    {columns.map((column, index) => {
                        if('value' in column) {
                            let ordering = 'sort';
                            if (order.indexOf(column.value) >= 0) {
                                ordering = 'sort-asc';
                            } else if (order.indexOf('-' + column.value) >= 0) {
                                ordering = 'sort-desc';
                            }

                            return (
                                <th key={`listGridHeader-${index}`}
                                       className="sortable"
                                       onClick={() => (this.handleSetOrder(column.value))}>
                                    <i className={`fa fa-${ordering} m-r-10`}></i>
                                    {column.label}
                                </th>
                            )
                        } else {
                            return (
                                <th key={`listGridHeader-${index}`}>{column.label}</th>
                            )
                        }
                    })}
                </tr>
            </thead>
        )
    }
    handleChangePage = page => {
        this.setState({isLoading: true});
        this.props.loadData(page, this.state.order)
            .then(() => (this.setState({isLoading: false})))
    };

    handleSetOrder(column) {
        const order = this.state.order;
        resolveNewOrderField(order, column);
        this.setState({order}, () => {
            this.handleChangePage(this.props.data.currentPage);
        });
    }
}

ListGrid.defaultProps = {
    rowAdditionalProps: {}
};