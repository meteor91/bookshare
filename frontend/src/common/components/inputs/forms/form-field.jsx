import React from 'react';


const FormField = ({label, errors, children}) => {
    const showError = errors.length>0;
    return (
        <div className="form-group">
            {label ? <label className="col-lg-3 control-label">{label}</label> : null}
            <div className="col-lg-9">
                {children}
            </div>
            <div className="col-lg-10">
                {showError ? <span className="help-block error-text">{errors[0]}</span> : null}
            </div>
        </div>
    )
};

FormField.propTypes = {
    label: React.PropTypes.string,
    errors: React.PropTypes.array,
};

FormField.defaultProps = {
    label: null,
    errors: []
};

export default FormField;