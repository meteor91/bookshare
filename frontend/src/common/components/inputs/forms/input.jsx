import React from 'react';

class FormInput extends React.Component {

    static propTypes = {
        onChange: React.PropTypes.func.isRequired,
        placeholder: React.PropTypes.string,
        label: React.PropTypes.string,
        value: React.PropTypes.any,
        errors: React.PropTypes.array,
    };

    render() {
        const {label, placeholder} = this.props;
        return (
            <div className="form-group">
                {label ? <label className="col-lg-2 control-label">{label}</label> : null}
                <div className="col-lg-10">
                    <input type="text" className="form-control"
                           placeholder={placeholder ? placeholder : ''}
                           onChange={this.handleChange} value={this.props.value}/>
                </div>
                <div className="col-lg-10">
                    {this.renderError()}
                </div>

            </div>
        )
    }

    renderError() {
        if(this.props.errors.length>0) {
            return (
                <span className="help-block error-text">{this.props.errors[0]}</span>
            )
        }
        return null;
    }

    handleChange = (e) => {
        const value = e.target.value;
        this.props.onChange(value);
    };
}

FormInput.defaultProps = {
    onBlur:  () => {},
    onFocus:  () => {},
    label: null,
    placeholder: null,
    errors: []
};

export default FormInput;