import React from 'react';
import classnames from 'classnames';
import _find from 'lodash/find';
import _debounce from 'lodash/debounce';

import {isDescendant} from 'utils/tools.js'


class SearchInput extends React.Component {

    state = {
        matches: [],
        value: '',
        displayDropdown: false
    };

    static propTypes = {
        searchHandler: React.PropTypes.func.isRequired,
        value: React.PropTypes.string.isRequired,
        handleChange: React.PropTypes.func.isRequired,
        onBlur:  React.PropTypes.func,
        onFocus:  React.PropTypes.func,
        fieldName: React.PropTypes.string,
        className: React.PropTypes.string,
        placeholder: React.PropTypes.string
    };

    componentWillMount() {
        document.addEventListener('mouseup', this.handleOuterClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mouseup', this.handleOuterClick);
    }

    render() {
        const {fieldName, className, placeholder, value} = this.props;
        const serachDropdownClasses = classnames({
            'dropdown-menu': true,
            'search-dropdown': true,
            'display-block ': this.state.matches.length > 0 && this.state.displayDropdown
        });

        return (
            <div ref="container">
                <input onChange={this.handleSearchInputChange}
                       type="text" className={className}
                       onBlur={this.onBlur} onFocus={this.onFocus}
                       placeholder={placeholder} value={value}/>

                <ul className={serachDropdownClasses}>
                    {this.state.matches.map((item, index) => (
                        <li onMouseDown={() => (this.handleSearchMatchSelect(item.id))}
                            key={`${fieldName}-matches-${index}`}>
                            <a>{item[fieldName]}</a>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }

    requestSearch = () => {
        const value = this.props.value;
        if(value.length>0) {
            this.props.searchHandler(value)
                .then(response => {
                    this.setState({matches: response});
                })
        } else {
            this.setState({matches: []});
        }
    };

    debouncedRequestSearch = _debounce(this.requestSearch, 500);

    handleOuterClick = (e) => {
        if (this.refs.container) {
            if (!isDescendant(e.target, this.refs.container, true)) {
                this.setState({
                    matches: []
                });
            }
        }
    };

    handleSearchInputChange = (e) => {
        const value = e.target.value;
        this.props.handleChange(value);
        this.debouncedRequestSearch();
    };

    handleSearchMatchSelect(id) {
        const selected = _find(this.state.matches, {id});
        this.setState({
            matches: [],
            value: selected[this.props.fieldName]
        });
        this.props.handleChange(selected[this.props.fieldName]);
    }

    onFocus = () => {
        this.setState({
            displayDropdown: true
        });
        this.props.onFocus();
    };

    onBlur = () => {
        this.props.onBlur();
    };
}

SearchInput.defaultProps = {
    onBlur:  () => {},
    onFocus:  () => {},
};

export default SearchInput;