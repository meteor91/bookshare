import React from 'react';


const Loader = ({height}) => {
    const margin = (height-24)/2;
    const loaderStyle = {
        marginTop: margin,
        marginBottom: margin,
    };

    return (
        <div className="loading-cont" style={loaderStyle}>
            <div className="loading-pulse">
            </div>
        </div>
    )
};

Loader.defaultProps = {
    height: 84
};

export default Loader;