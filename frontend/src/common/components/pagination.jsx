import React from 'react';
import classnames from 'classnames';


export default class Pagination extends React.Component {
    static propTypes = {
        pagesCount: React.PropTypes.number.isRequired,
        currentPage: React.PropTypes.number.isRequired,
        changePage: React.PropTypes.func.isRequired
    };

    handlePageClick(page, e) {
        e.preventDefault();
        if(page>=1 && page<=this.props.pagesCount) {
            this.props.changePage(page);
        }
    }

    getPageLink(page) {
        const linkClass = classnames({
            'active': page==this.props.currentPage
        });
        return (
            <li className={linkClass} onClick={this.handlePageClick.bind(this, page)} key={`pagination-page-${page}`}>
                <a href="#">{page}</a>
            </li>
        )
    }

    render() {
        // убрать хардкод, сделать настраиваемым
        const pageLinks = [];
        const {pagesCount, currentPage} = this.props;
        if(pagesCount>6) {
            pageLinks.push(this.getPageLink(1));
            pageLinks.push(this.getPageLink(2));
            pageLinks.push(this.getPageLink(3));
            if(currentPage>2 && currentPage < pagesCount-1) {
                if(currentPage>5) {
                    pageLinks.push(<li className="disabled" key={`pagination-page-left`}><a href="#">...</a></li>);
                }
                const prev = currentPage - 1,
                      next = currentPage + 1;
                if(prev>3) {
                    pageLinks.push(this.getPageLink(prev));
                }
                if(currentPage>3 && currentPage<pagesCount-2) {
                    pageLinks.push(this.getPageLink(currentPage));
                }
                if(next<pagesCount-2) {
                    pageLinks.push(this.getPageLink(next));
                }

                if(currentPage<pagesCount-4) {
                    pageLinks.push(<li className="disabled" key={`pagination-page-right`}><a href="#">...</a></li>);
                }
            } else  {
                pageLinks.push(<li className="disabled" key={`pagination-page-center`}><a href="#">...</a></li>);
            }
            pageLinks.push(this.getPageLink(pagesCount-2));
            pageLinks.push(this.getPageLink(pagesCount-1));
            pageLinks.push(this.getPageLink(pagesCount));
        } else {
            for (var i = 1; i <= pagesCount; i++) {
                pageLinks.push(this.getPageLink(i));
            }
        }
        const nextButtonClass = classnames({'disabled': currentPage==pagesCount}),
              prevButtonClass = classnames({'disabled': currentPage==1});
        return (
            <ul className="pagination">
                <li onClick={this.handlePageClick.bind(this, currentPage-1)} className={prevButtonClass}>
                    <a href="#">«</a>
                </li>
                {pageLinks}
                <li onClick={this.handlePageClick.bind(this, currentPage+1)} className={nextButtonClass}>
                    <a href="#">»</a>
                </li>
            </ul>
        )
    }
    
}