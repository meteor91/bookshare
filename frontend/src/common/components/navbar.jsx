import React from 'react';
import {connect} from 'react-redux';
import {Link, browserHistory} from 'react-router';
import classnames from 'classnames';

import {displayAuthDialog, logout} from 'apps/auth/action-creators';
import LoginForm from 'apps/auth/components/login-form';

class Navbar extends React.Component {
    state = {
        authDropdownOpened: false
    };

    render() {
        return (
            <div className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        <Link to="/" className="navbar-brand">Bookshare</Link>
                        <button className="navbar-toggle" type="button">
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                    </div>
                    <div className="navbar-collapse collapse" id="navbar-main">
                        {/*<ul className="nav navbar-nav">*/}
                        {/*</ul>*/}

                        <ul className="nav navbar-nav navbar-right">
                            {this.renderMenu()}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }

    renderMenu() {
        if(!this.props.authorized) {
            return (
                <li>
                    <a onClick={this.loginDialogShow}>Войти</a>
                    <LoginForm display={this.props.displayAuthDialog}
                               handleClose={this.loginDialogClose}/>
                </li>
            )
        }
        const authDropdownClass = classnames({
            dropdown: true,
            open: this.state.authDropdownOpened
        });
        return (
            <li className={authDropdownClass}>
                <a className="dropdown-toggle ust" onClick={this.toggleAuthDropdown}>
                    {this.props.userName}<span className="caret"></span>
                </a>
                <ul className="dropdown-menu">
                    <li><Link onClick={this.closeDropdown} to="/profile/">Профиль</Link></li>
                    <li className="divider"></li>
                    <li><a onClick={this.handleLogout}>Выход</a></li>
                </ul>
            </li>
        )
    }

    toggleAuthDropdown = (e) => {
        e.preventDefault();
        this.setState({
            authDropdownOpened: !this.state.authDropdownOpened
        })
    };

    handleLogout = () => {
        this.props.logout();
        this.closeDropdown();
        browserHistory.push('/');
    };

    closeDropdown = () => {
        this.setState({
            authDropdownOpened: false
        });
    };

    loginDialogShow = () => {
        this.props.setDisplayAuthDialog(true);
    };

    loginDialogClose = () => {
        this.props.setDisplayAuthDialog(false);
    };
}

export default connect(
    store => ({
        authorized: store.auth.authorized,
        displayAuthDialog: store.auth.displayAuthDialog,
        userName: store.profile.user.name
    }),
    dispatch => ({
        logout: () => (dispatch(logout())),
        setDisplayAuthDialog: state => (dispatch(displayAuthDialog(state)))
    })
)(Navbar)