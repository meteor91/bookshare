import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {connect} from 'react-redux';

import {displayAuthDialog} from 'apps/auth/action-creators';
import {displayAddBookDialog} from 'apps/profile/action-creators';


export default class ModalLayout extends React.Component {
    render() {
        const transition = {
            transitionName: "fade",
            transitionEnterTimeout: 300,
            transitionLeaveTimeout: 300
        };

        return (
            <ReactCSSTransitionGroup {...transition}>
                {this.props.display ? (
                    <div className='modal display-block' onClick={this.props.handleClose}>
                        <div className='modal-dialog active'>
                            <div className="modal-content">
                                { this.props.children }
                            </div>
                        </div>
                    </div>
                ): null}
            </ReactCSSTransitionGroup>
        )
    }
}