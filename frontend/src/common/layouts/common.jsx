import React from 'react';
import {connect} from 'react-redux';
import _map from 'lodash/map';

import Navbar from 'common/components/navbar';
import Alert from 'apps/alerts/components/alert';
import {closeAlert} from 'apps/alerts/action-creators';

import {displayAuthDialog} from 'apps/auth/action-creators';
import {displayAddBookDialog} from 'apps/profile/action-creators';


class CommonLayout extends React.Component {

    renderAlerts() {
        return _map(this.props.alerts, (item, index) => (
            <Alert message={item.message}
                   handleClose={() => (this.props.closeAlert(index))}
                   type={item.type} key={`alert-${index}`}/>
        ))
    }

    render() {
        return (
            <div>
                <Navbar/>
                <div className="container container-offset">
                    <div className="row">
                        {this.renderAlerts()}
                    </div>
                    { this.props.children }
                </div>
            </div>
        )
    }
}

export default connect(
    store => ({
        alerts: store.alerts
    }),
    dispatch => ({
        closeAlert: id => dispatch(closeAlert(id))
    })
)(CommonLayout)