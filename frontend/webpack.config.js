// http://webpack.github.io/docs/configuration.html
// http://webpack.github.io/docs/webpack-dev-server.html
var app_root = 'src'; // the app root folder: src, src_users, etc
var path = require('path');
var CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    app_root: app_root, // the app root folder, needed by the other webpack configs
    entry: [
        // http://gaearon.github.io/react-hot-loader/getstarted/
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        __dirname + '/' + app_root + '/app.js',
    ],
    output: {
        // path: __dirname + '/public/js',
        path: '../backend/staticfiles/js/',
        publicPath: 'js/',
        filename: 'bundle.js',
    },
    resolve: {
        root: [
            __dirname,
            path.join(__dirname, 'src'),
            path.join(__dirname, 'node_modules')
        ],
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loaders: ['react-hot', 'babel'],
                exclude: /node_modules/,
            }
        ],
    },
    devtool: 'source-map',
    devServer: {
        contentBase: __dirname + '/dist',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },
    plugins: [
        new CleanWebpackPlugin(['js/bundle.js'], {
            root: __dirname + '/public',
            verbose: true,
            dry: false, // true for simulation
        }),
    ],

    externals: {
        envVariables: `{API_URL: 'http://localhost:8000', env: 'dev'}`,
    }
};
