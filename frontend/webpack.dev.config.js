var webpack = require("webpack");
//var ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = require('./webpack.config.js');    // inherit from the main config file

// disable the hot reload
module.exports.entry = [
    __dirname + '/' + module.exports.app_root + '/app.js'
];