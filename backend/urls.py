from django.conf import settings

from rest_framework_jwt.views import obtain_jwt_token
from django.conf.urls import url, include
from django.contrib import admin

from books.views import AppView

urlpatterns = [
    # url(r'^api-token-auth/', obtain_jwt_token),

    url(r'^api/user/', include('users.urls')),
    url(r'^api/books/', include('books.urls')),
    url(r'^api/lends/', include('lends.urls')),

    url(r'^docs/', include('rest_framework_docs.urls')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

urlpatterns += [
    url(r'^', AppView.as_view()),
]
