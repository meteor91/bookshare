import datetime

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
}

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=60 * 60 * 24 * 7),
    'JWT_PAYLOAD_HANDLER': 'users.utils.jwt_payload_handler',
    'JWT_PAYLOAD_GET_USERNAME_HANDLER': 'users.utils.jwt_get_username_from_payload_handler',
}
