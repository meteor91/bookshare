import dj_database_url

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bookshare',
        'USER': 'postgres',
        'PASSWORD': None,
        'HOST': 'localhost',
        'PORT': 5432,
    },
}

# Update database configuration with $DATABASE_URL.
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)