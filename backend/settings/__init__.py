from .base import *
from .databases import *
from .i18n import *
from .middlewares import *
from .rest import *
from .static import *

try:
    env = os.environ['ENV']
    if env == 'prod':
        from .environments.production import *
except KeyError:
    CORS_ORIGIN_ALLOW_ALL = True
    from .environments.development import *
