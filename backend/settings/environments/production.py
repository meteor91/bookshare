import os


RAVEN_CONFIG = {
    'dsn': os.environ['sentry_dsn']
}

DEBUG = False