from backend.settings import INSTALLED_APPS
from backend.settings.middlewares import MIDDLEWARE_CLASSES

INSTALLED_APPS.append('debug_toolbar')
MIDDLEWARE_CLASSES.append('debug_toolbar.middleware.DebugToolbarMiddleware')
DEBUG = True
