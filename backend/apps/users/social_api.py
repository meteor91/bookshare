import requests
import json

from users.exceptions import IncorrectSocialToken


class VK_API:
    version = '5.60'
    ACCESS_TOKEN_URL = 'https://oauth.vk.com/access_token'
    CHECK_USER_TOKEN_URL = 'https://api.vk.com/method/secure.checkToken'
    GET_USER_INFO = 'https://api.vk.com/method/users.get'

    def __init__(self, vk_app, version=None):
        self.vk_app = vk_app
        if version:
            self.version = version

    def get_app_access_token(self):
        response = requests.get(self.ACCESS_TOKEN_URL, params={
            'client_id': self.vk_app.client_id,
            'client_secret': self.vk_app.secret,
            'version': self.version,
            'grant_type': 'client_credentials'
        })

        result = json.loads(response.text)
        return result['access_token']

    def check_user_token(self, user_access_token):
        app_access_token = self.get_app_access_token()
        response = requests.get(self.CHECK_USER_TOKEN_URL, params={
            'token': user_access_token,
            'access_token': app_access_token,
            'client_secret': self.vk_app.secret
        })

        result = json.loads(response.text)
        if result['response']['success'] != 1:
            raise IncorrectSocialToken()
        return result['response']['user_id'], result['response']['expire']

    def get_user_name(self, user_id):
        response = requests.get(self.GET_USER_INFO, params={'user_ids': user_id})
        result = json.loads(response.text)
        user_data = result['response'][0]
        return '%s %s' % (user_data['first_name'], user_data['last_name'])


class FB_API:
    CHECK_USER_TOKEN_URL = 'https://graph.facebook.com/debug_token'
    ACCESS_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token'
    GET_USER_INFO = 'https://graph.facebook.com/v2.8/'

    def __init__(self, fb_app):
        self.fb_app = fb_app

    def get_app_access_token(self):
        response = requests.get(self.ACCESS_TOKEN_URL, params={
            'client_id': self.fb_app.client_id,
            'client_secret': self.fb_app.secret,
            'grant_type': 'client_credentials'
        })

        result = response.text.split('=')
        if len(result) == 2 and result[0] == 'access_token':
            return result[1]

    def check_user_token(self, user_access_token):
        app_access_token = self.get_app_access_token()
        response = requests.get(self.CHECK_USER_TOKEN_URL, params={
            'input_token': user_access_token,
            'access_token': app_access_token
        })

        result = json.loads(response.text)
        if result['data']['is_valid'] is not True:
            raise IncorrectSocialToken()
        return result['data']['user_id'], result['data']['expires_at']

    def get_user_name(self, user_id, access_token):
        response = requests.get(self.GET_USER_INFO + user_id, params={'access_token': access_token})
        result = json.loads(response.text)
        return result['name']
