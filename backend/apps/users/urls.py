from django.conf.urls import url
from rest_framework_jwt.views import verify_jwt_token

from .views import SocialAuthView


urlpatterns = [
    url(r'^social/auth/$', SocialAuthView.as_view()),
    url(r'^api-token-verify/$', verify_jwt_token),
]
