from __future__ import absolute_import


from django.db import models
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.encoding import force_unicode as force_text


class Providers:
    VK = 'vk'
    FB = 'fb'

providers = (
    (Providers.VK, 'Vkontakte'),
    (Providers.FB, 'Facebook'),
)


class SocialApp(models.Model):
    provider = models.CharField(verbose_name=_('provider'), max_length=30, choices=providers)
    name = models.CharField(verbose_name=_('name'), max_length=40)
    client_id = models.CharField(verbose_name=_('client id'), max_length=191)
    secret = models.CharField(verbose_name=_('secret key'), max_length=191)
    key = models.CharField(verbose_name=_('key'), max_length=191, blank=True)

    class Meta:
        verbose_name = _('social application')
        verbose_name_plural = _('social applications')

    def __str__(self):
        return self.name


class SocialAccount(models.Model):
    user = models.ForeignKey('users.User')
    provider = models.CharField(verbose_name=_('provider'), max_length=30, choices=providers)
    uid = models.CharField(verbose_name=_('uid'), max_length=255)
    last_login = models.DateTimeField(verbose_name=_('last login'), auto_now=True)
    date_joined = models.DateTimeField(verbose_name=_('date joined'), auto_now_add=True)

    class Meta:
        unique_together = ('provider', 'uid')
        verbose_name = _('social account')
        verbose_name_plural = _('social accounts')

    def authenticate(self):
        return authenticate(account=self)

    def __str__(self):
        return force_text(self.user)

    def get_profile_url(self):
        return self.get_provider_account().get_profile_url()

    def get_avatar_url(self):
        return self.get_provider_account().get_avatar_url()

    def get_provider(self):
        return providers.registry.by_id(self.provider)

    def get_provider_account(self):
        return self.get_provider().wrap_account(self)


class SocialToken(models.Model):
    app = models.ForeignKey(SocialApp)
    account = models.ForeignKey(SocialAccount)
    token = models.TextField(verbose_name=_('token'))
    token_secret = models.TextField(blank=True, verbose_name=_('token secret'))
    expires_at = models.DateTimeField(blank=True, null=True, verbose_name=_('expires at'))

    class Meta:
        unique_together = ('app', 'account')
        verbose_name = _('social application token')
        verbose_name_plural = _('social application tokens')

    def __str__(self):
        return self.token
