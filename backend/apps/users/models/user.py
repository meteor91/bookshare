from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, login, password=None):
        user = self.model(login=login)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, login, password):
        user = self.model(login=login, is_superuser=True)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    login = models.CharField('Логин', max_length=255, unique=True)
    name = models.CharField('Имя', max_length=255)
    is_active = models.BooleanField('Активный', default=True)
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)
    USERNAME_FIELD = 'login'

    class Meta:
        verbose_name = 'Пользовател'
        verbose_name_plural = 'Пользователи'

    @property
    def is_staff(self):
        return self.is_superuser

    objects = CustomUserManager()

    def __str__(self):
        return self.name or self.login

    def get_short_name(self):
        return self.login

    def get_full_name(self):
        return self.login