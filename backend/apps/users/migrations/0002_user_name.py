# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-11-20 11:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='name',
            field=models.CharField(default='', max_length=255, verbose_name='Имя'),
            preserve_default=False,
        ),
    ]
