from calendar import timegm
from datetime import datetime

from rest_framework_jwt.settings import api_settings
from .models import SocialAccount, SocialApp, Providers

from .models import User
from .social_api import VK_API, FB_API

jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


def jwt_payload_handler(user):
    payload = {
        'login': user.login,
        'name': user.name,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
    }

    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload


def jwt_get_username_from_payload_handler(payload):
    return payload.get('login')


def auth_with_vk(access_token):
    try:
        vk_app = SocialApp.objects.get(name='vk_app_main')
        vk_api = VK_API(vk_app)
        user_id, expires = vk_api.check_user_token(access_token)
        try:
            social_account = SocialAccount.objects.get(uid=user_id)
            user = social_account.user
        except SocialAccount.DoesNotExist:
            user_name = vk_api.get_user_name(user_id)
            user = User.objects.create(
                login='vk-%s' % user_id,
                name=user_name
            )
            SocialAccount.objects.create(
                uid=user_id,
                provider=Providers.VK,
                user=user
            )
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return token
    except (SocialApp.DoesNotExist, SocialApp.MultipleObjectsReturned):
        return {}


def auth_with_fb(access_token):
    try:
        fb_app = SocialApp.objects.get(name='fb_app_main')
        fb_api = FB_API(fb_app)
        user_id, expires = fb_api.check_user_token(access_token)
        try:
            social_account = SocialAccount.objects.get(uid=user_id)
            user = social_account.user
        except SocialAccount.DoesNotExist:
            user_name = fb_api.get_user_name(user_id, access_token)
            user = User.objects.create(
                login='fb-%s' % user_id,
                name=user_name
            )
            SocialAccount.objects.create(
                uid=user_id,
                provider=Providers.FB,
                user=user
            )
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return token
    except (SocialApp.DoesNotExist, SocialApp.MultipleObjectsReturned):
        return {}
