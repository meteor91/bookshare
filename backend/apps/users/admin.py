from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django import forms

from .forms import CustomUserCreationForm
from .models import User, SocialAccount, SocialApp, SocialToken


@admin.register(User)
class UserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    list_display = 'login', 'is_active', 'is_superuser', 'created_at', 'last_login'
    fieldsets = (
        (None, {'fields': ('login', 'name', 'is_active', 'is_superuser', 'password')}),
    )
    ordering = 'login',
    list_filter = 'is_superuser',
    search_fields = 'login',


admin.site.unregister(Group)


class SocialAppForm(forms.ModelForm):
    class Meta:
        model = SocialApp
        exclude = []
        widgets = {
            'client_id': forms.TextInput(attrs={'size': '100'}),
            'key': forms.TextInput(attrs={'size': '100'}),
            'secret': forms.TextInput(attrs={'size': '100'})
        }


class SocialAppAdmin(admin.ModelAdmin):
    form = SocialAppForm
    list_display = ('name', 'provider',)


class SocialAccountAdmin(admin.ModelAdmin):
    search_fields = []
    raw_id_fields = ('user',)
    list_display = ('user', 'uid', 'provider')
    list_filter = ('provider',)


class SocialTokenAdmin(admin.ModelAdmin):
    raw_id_fields = ('app', 'account',)
    list_display = ('app', 'account', 'truncated_token', 'expires_at')
    list_filter = ('app', 'app__provider', 'expires_at')

    def truncated_token(self, token):
        max_chars = 40
        ret = token.token
        if len(ret) > max_chars:
            ret = ret[0:max_chars] + '...(truncated)'
        return ret
    truncated_token.short_description = 'Token'

admin.site.register(SocialApp, SocialAppAdmin)
admin.site.register(SocialToken, SocialTokenAdmin)
admin.site.register(SocialAccount, SocialAccountAdmin)