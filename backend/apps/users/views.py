from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from users.serializers import SocialAuthSerializer
from .utils import auth_with_vk, auth_with_fb
from .exceptions import IncorrectSocialToken


class SocialAuthView(generics.GenericAPIView):
    """Проверка токена, отправленного из браузера пользователя после клиенткой авторизации"""
    serializer_class = SocialAuthSerializer
    permission_classes = AllowAny,

    def post(self, request, *args, **kwargs):
        auth_info = SocialAuthSerializer(request.data)
        try:
            if auth_info.data['provider'] == 'vk':
                token = auth_with_vk(auth_info.data['access_token'])
            elif auth_info.data['provider'] == 'fb':
                token = auth_with_fb(auth_info.data['access_token'])
            return Response(data={'token': token})
        except IncorrectSocialToken:
            return Response(data={'error': 'invalid_token'})
