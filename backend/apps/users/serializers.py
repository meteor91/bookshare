from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = 'id', 'name'


class SocialAuthSerializer(serializers.Serializer):
    access_token = serializers.CharField()
    provider = serializers.CharField()
