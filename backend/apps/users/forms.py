from django.contrib.auth.forms import UserCreationForm

from .models import User


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        fields = "login", 'is_active', 'is_superuser',
        model = User
