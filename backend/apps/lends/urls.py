from django.conf.urls import url
from .views import CreateLendView, CurrentUserLendsListView, LendFinishView


urlpatterns = [
    url(r'^create/$', CreateLendView.as_view()),
    url(r'^list/$', CurrentUserLendsListView.as_view()),
    url(r'^finish/(?P<pk>\d+)/$', LendFinishView.as_view())
]