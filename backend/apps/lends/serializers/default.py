from rest_framework import serializers
from datetime import timedelta, datetime

from books.models import UserBook

from books.serializers.base import UserBookPreviewSerializer
from ..models import Lend


class LendCreateSerializer(serializers.ModelSerializer):
    receiver = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    user_book = serializers.PrimaryKeyRelatedField(queryset=UserBook.objects)

    class Meta:
        model = Lend
        fields = 'user_book', 'receiver'


class LendFinishSerializer(serializers.ModelSerializer):

    class Meta:
        model = Lend
        fields = 'id', 'finished', 'finished_at'
        read_only_fields = 'id', 'finished_at',

    def save(self, **kwargs):
        lend = self.instance
        if not lend.finished_at or not lend.finished:
            lend.finished = True
            lend.finished_at = datetime.now()
            lend.save()

        return self.instance


class LendSerializer(serializers.ModelSerializer):
    user_book = UserBookPreviewSerializer(read_only=True)
    last_return_date = serializers.SerializerMethodField()

    class Meta:
        model = Lend
        fields = 'id', 'user_book', 'begin_at', 'finished', 'finished_at', 'last_return_date'

    def get_last_return_date(self, obj):
        return obj.begin_at + timedelta(days=obj.user_book.max_lend_days)