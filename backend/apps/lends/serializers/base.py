from rest_framework import serializers
from datetime import timedelta

from users.serializers import UserSerializer
from ..models import Lend


class LendPreviewSerializer(serializers.ModelSerializer):
    latest_return_date = serializers.SerializerMethodField()
    receiver = UserSerializer(read_only=True)

    class Meta:
        model = Lend
        fields = 'id', 'begin_at', 'finished', 'finished_at', 'latest_return_date', 'receiver'

    def get_latest_return_date(self, obj):
        return obj.begin_at + timedelta(days=obj.user_book.max_lend_days)


