from django.db import models


class Lend(models.Model):
    receiver = models.ForeignKey('users.User', verbose_name='Получатель', related_name='received_lends')
    user_book = models.ForeignKey('books.UserBook', verbose_name='Книга пользователя')
    begin_at = models.DateTimeField('Дата получения', auto_now_add=True)
    finished = models.BooleanField('Завершена', default=False)
    finished_at = models.DateTimeField('Дата завершения', null=True, blank=True)

    class Meta:
        verbose_name = 'Одолженная книга'
        verbose_name_plural = 'Одолженные книги'

    def __str__(self):
        return '%s получил "%s" у пользователя %s' % (self.receiver, self.user_book.book, self.user_book.user)
