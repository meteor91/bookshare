from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView

from common.paginations import StandardResultsSetPagination
from .serializers import LendCreateSerializer, LendSerializer, LendFinishSerializer
from .models import Lend


class CreateLendView(CreateAPIView):
    """Метод для взятия книги"""
    serializer_class = LendCreateSerializer
    queryset = Lend.objects


class CurrentUserLendsListView(ListAPIView):
    """Список взятых пользователем книг"""
    serializer_class = LendSerializer
    pagination_class = StandardResultsSetPagination
    order_fields = 'user_book__book__author__name', '-user_book__book__author__name',\
                   'user_book__book__name', '-user_book__book__name',\
                   'user_book__user__name', '-user_book__user__name',\
                   'begin_at', '-begin_at', 'finished_at', '-finished_at'

    def get_order_fields_from_request(self):
        order_fields = []
        for param in self.request.query_params.get('order', '').split(','):
            if param in self.order_fields:
                order_fields.append(param)
        return order_fields

    def get_queryset(self):
        finished = self.request.query_params.get('finished', None)
        if finished is not None:
            queryset = Lend.objects.filter(receiver=self.request.user, finished=(finished == 'true'))
        else:
            queryset = Lend.objects.filter(receiver=self.request.user)

        order_fields = self.get_order_fields_from_request()
        if order_fields:
            queryset = queryset.order_by(*order_fields)
        return queryset


class LendFinishView(UpdateAPIView):
    """Метод для возврата книги"""
    serializer_class = LendFinishSerializer

    def get_queryset(self):
        return Lend.objects.filter(receiver=self.request.user)
