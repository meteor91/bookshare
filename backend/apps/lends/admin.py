from django.contrib import admin

from .models import Lend


@admin.register(Lend)
class LendAdmin(admin.ModelAdmin):
    list_display = 'receiver', 'user_book', 'begin_at'
