from django.db import models
from django.apps import apps


class BooksManager(models.Manager):

    def get_available(self, user=None):
        UserBook = apps.get_model('books', 'UserBook')
        available_user_books = UserBook.objects.filter(available=True).values_list('book_id', flat=True)
        if user:
            available_user_books = available_user_books.exclude(user=user)
        return self.filter(id__in=available_user_books)
