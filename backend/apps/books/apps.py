from django.apps import AppConfig


class BooksConfig(AppConfig):
    name = 'backend.apps.books'
