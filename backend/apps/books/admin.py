from django.contrib import admin

from .models import Author, Book, UserBook


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = 'name',


class UserBookInline(admin.TabularInline):
    model = UserBook
    extra = 0


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = 'author', 'name',
    list_display_links = 'name',
    inlines = UserBookInline,


@admin.register(UserBook)
class UserBookAdmin(admin.ModelAdmin):
    list_display = 'user', 'book', 'available'
    list_editable = 'available',

