from django.views.generic import TemplateView
from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from common.paginations import StandardResultsSetPagination
from .models import Book, UserBook, Author
from .serializers import AvailableBookSerializer, UserBookSerializer, AuthorSerializer, \
    BookSearchSerializer, CreateUserBookSerializer, BookDetailSerializer


class AppView(TemplateView):
    template_name = 'books/app.html'


class AvailableBooksListView(ListAPIView):
    """Список доступных книг"""
    serializer_class = AvailableBookSerializer
    pagination_class = StandardResultsSetPagination
    permission_classes = AllowAny,
    order_fields = 'name', '-name', 'author__name', '-author__name'

    def get_order_fields_from_request(self):
        order_fields = []
        for param in self.request.query_params.get('order', '').split(','):
            if param in self.order_fields:
                order_fields.append(param)
        return order_fields

    def get_queryset(self):
        if self.request.user.id:
            queryset = Book.objects.get_available(user=self.request.user).order_by('author__name')
        else:
            queryset = Book.objects.get_available()
        order_fields = self.get_order_fields_from_request()
        if order_fields:
            queryset = queryset.order_by(*order_fields)
        return queryset


class BookDetailView(RetrieveAPIView):
    """Информация о конкретной книге и всех пользователях, готовых предоставить данную кингу"""
    permission_classes = AllowAny,
    serializer_class = BookDetailSerializer
    queryset = Book.objects


class UserBooksListView(ListAPIView):
    """Список книг добавленных пользователем"""
    serializer_class = UserBookSerializer
    pagination_class = StandardResultsSetPagination
    authentication_classes = JSONWebTokenAuthentication,
    http_method_names = 'get',
    order_fields = 'book__name', '-book__name', 'created_at', '-created_at',\
                   'book__author__name', '-book__author__name'

    def get_order_fields_from_request(self):
        order_fields = []
        for param in self.request.query_params.get('order', '').split(','):
            if param in self.order_fields:
                order_fields.append(param)
        return order_fields

    def get_queryset(self):
        queryset = UserBook.objects.filter(user=self.request.user).order_by('book__name')
        order_fields = self.get_order_fields_from_request()
        if order_fields:
            queryset = queryset.order_by(*order_fields)

        return queryset


class SearchAuthorByNameView(ListAPIView):
    """Поиск автора по имени"""
    serializer_class = AuthorSerializer
    permission_classes = AllowAny,
    http_method_names = 'get',

    def get_queryset(self):
        name = self.kwargs['name']
        return Author.objects.filter(name__icontains=name).order_by('name')[:5]


class SearchBookByNameView(ListAPIView):
    """Поиск книги по названию"""
    serializer_class = BookSearchSerializer
    permission_classes = AllowAny,
    http_method_names = 'get',

    def get_queryset(self):
        name = self.kwargs['name']
        return Book.objects.filter(name__icontains=name).order_by('name')[:5]


class CreateUserBookView(GenericAPIView):
    """Добавление книги пользователем по автору и названию"""
    serializer_class = CreateUserBookSerializer
    http_method_names = 'post', 'put'
    queryset = UserBook.objects

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create_user_book()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request, *args, **kwargs):
        user_book = self.get_object()
        lend = user_book.lend_set.last()
        if lend and lend.finished is False:
            return Response(status=status.HTTP_423_LOCKED)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.update_user_book(user_book)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)