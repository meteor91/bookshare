# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-12-19 19:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='book',
            options={'verbose_name': 'Книга', 'verbose_name_plural': 'Книги'},
        ),
        migrations.AlterModelOptions(
            name='userbook',
            options={'verbose_name': 'Книга пользователя', 'verbose_name_plural': 'Книги пользователей'},
        ),
    ]
