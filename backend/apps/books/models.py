from django.db import models

from .manager import BooksManager


class Author(models.Model):
    name = models.CharField('Автор', max_length=255)
    entry_added_by = models.ForeignKey('users.User', verbose_name='Запись добавена пользователем')

    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def __str__(self):
        return self.name


class Book(models.Model):
    name = models.CharField('Название книги', max_length=255)
    author = models.ForeignKey('books.Author', verbose_name='Автор')
    entry_added_by = models.ForeignKey('users.User', verbose_name='Запись добавена пользователем')

    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    objects = BooksManager()

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'

    def __str__(self):
        return self.name


class UserBook(models.Model):
    book = models.ForeignKey('books.Book', verbose_name='Книга')
    user = models.ForeignKey('users.User', verbose_name='Пользователь')
    max_lend_days = models.PositiveIntegerField('Максимальное кол-во дней для одалживания')
    available = models.BooleanField('Доступна', default=True)

    created_at = models.DateTimeField('Дата создания', auto_now_add=True)
    updated_at = models.DateTimeField('Дата последнего изменения', auto_now=True)

    class Meta:
        verbose_name = 'Книга пользователя'
        verbose_name_plural = 'Книги пользователей'

    def __str__(self):
        return '%s - %s' % (self.user, self.book)
