from rest_framework import serializers
from datetime import timedelta

from users.serializers import UserSerializer
from lends.models import Lend
from lends.serializers.base import LendPreviewSerializer

from ..models import Book, Author, UserBook
from .base import AuthorSerializer, BookSerializer


class UserBookUserInfoSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = UserBook
        fields = 'id', 'user', 'max_lend_days'


class BookDetailSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)
    user_book_info = serializers.SerializerMethodField()

    class Meta:
        model = Book
        fields = 'id', 'name', 'author', 'user_book_info'

    def get_user_book_info(self, obj):
        userbook_set = obj.userbook_set.filter(available=True)
        results = []
        for user_book in userbook_set:
            try:
                lend = user_book.lend_set.get(finished=False)
                book_info = {
                    'user_book': UserBookUserInfoSerializer(user_book).data,
                    'available': False,
                    'date_free': lend.begin_at + timedelta(days=user_book.max_lend_days)
                }
                if self.context['request'].user.id:
                    book_info['lend_by_me'] = lend.receiver == self.context['request'].user
                else:
                    book_info['lend_by_me'] = False
                results.append(book_info)
            except Lend.DoesNotExist:
                results.append({
                    'user_book': UserBookUserInfoSerializer(user_book).data,
                    'available': True
                })
            except Lend.MultipleObjectsReturned:
                # send system error somewhere
                pass
        return results


class UserBookSerializer(serializers.ModelSerializer):
    book = BookSerializer(read_only=True)
    user = UserSerializer(read_only=True)
    lend = serializers.SerializerMethodField()

    class Meta:
        model = UserBook
        fields = 'id', 'book', 'user', 'lend', 'max_lend_days'

    def get_lend(self, obj):
        lend = obj.lend_set.last()
        if lend and lend.finished is False:
            lend_serializer = LendPreviewSerializer(lend)
            return lend_serializer.data
        return None


class CreateUserBookSerializer(serializers.Serializer):
    book_name = serializers.CharField(allow_blank=False, required=True)
    author_name = serializers.CharField(allow_blank=False, required=True)
    max_lend_days = serializers.IntegerField(required=True)
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    def create_user_book(self):
        return self.create(self._validated_data)

    def update_user_book(self, user_book):
        return self.update(user_book, self._validated_data)

    def create(self, validated_data):
        user = validated_data.get('user')
        author = CreateUserBookSerializer.process_author(validated_data.get('author_name'), user)
        book = CreateUserBookSerializer.process_book(author, validated_data.get('book_name'), user)

        user_book = UserBook.objects.create(
            book=book,
            max_lend_days=validated_data.get('max_lend_days'),
            user=user
        )

        return user_book

    def update(self, user_book, validated_data):
        user = validated_data.get('user')
        author = CreateUserBookSerializer.process_author(validated_data.get('author_name'), user)
        book = CreateUserBookSerializer.process_book(author, validated_data.get('book_name'), user)

        user_book.book = book
        user_book.max_lend_days = validated_data.get('max_lend_days')
        user_book.save()

        return user_book

    @staticmethod
    def process_author(author_name, user):
        try:
            author = Author.objects.get(name=author_name)
        except Author.DoesNotExist:
            author = Author.objects.create(
                name=author_name,
                entry_added_by=user
            )
        return author

    @staticmethod
    def process_book(author, book_name, user):
        try:
            book = Book.objects.get(author=author, name=book_name)
        except Book.DoesNotExist:
            book = Book.objects.create(
                author=author,
                name=book_name,
                entry_added_by=user
            )
        return book