from rest_framework import serializers

from users.serializers import UserSerializer
from ..models import Book, Author, UserBook


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = 'id', 'name'


class BookSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = 'id', 'name'


class BookSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)

    class Meta:
        model = Book
        fields = 'id', 'name', 'author', 'created_at'


class AvailableBookSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)

    class Meta:
        model = Book
        fields = 'id', 'name', 'author',


class UserBookPreviewSerializer(serializers.ModelSerializer):
    book = BookSerializer(read_only=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = UserBook
        fields = 'id', 'book', 'user'
