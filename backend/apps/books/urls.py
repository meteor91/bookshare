from django.conf.urls import url
from .views import (
    AvailableBooksListView,
    UserBooksListView,
    SearchAuthorByNameView,
    SearchBookByNameView,
    CreateUserBookView,
    BookDetailView
)

urlpatterns = [
    url(r'^available/$', AvailableBooksListView.as_view()),
    url(r'^user/$', UserBooksListView.as_view()),
    url(r'^author/search/(?P<name>.+)/$', SearchAuthorByNameView.as_view()),
    url(r'^search/(?P<name>.+)/$', SearchBookByNameView.as_view()),
    url(r'^create/$', CreateUserBookView.as_view()),
    url(r'^update/(?P<pk>\d+)/$', CreateUserBookView.as_view()),
    url(r'^book/(?P<pk>\d+)/$', BookDetailView.as_view())

]